# Setup a self manged kubernetes cluster on Raspberry Pis

[[_TOC_]]

## Setup Raspberry Pis

Install Ubuntu Server LTS 20.04 on SD card (Raspberry PI Imager).
We used a 128 GB micro SD card. When using 32 GB SD card we observed stability problems.

### After OS install

```bash
# Update system
apt get update
apt get upgrade
```
 
### Prepare microk8s
```bash
# Do following on all nodes:

sudo snap install microk8s --classic --channel=1.21
# Follow steps at https://microk8s.io/docs/install-alternatives#heading--arm
sudo reboot now
# Change permissions
sudo usermod -a -G microk8s $USER
sudo chown -f -R $USER ~/.kube
su - $USER
```

### Setup cluster
```bash
# Run on master:
microk8s add-node

# Copy output and run command on all other slave nodes
```

### Install microk8s addons
These addons are required to provide all services required for a stable cluster configuration.

```bash
# K8S Dashboard (optional)
microk8s enable dashboard
# Router / Ingress
microk8s enable traefik
# K8S system services
microk8s enable storage
microk8s enable dns
microk8s enable registry
# Helm 3 for application mangement
microk8s enable helm3

# Setup helm
# https://stackoverflow.com/questions/65407317/helm-init-failed-is-not-a-valid-chart-repository-or-cannot-be-reached-failed-to
microk8s helm init --stable-repo-url=https://charts.helm.sh/stable
```

## Configure external cluster access

### Config Traefik
https://www.robert-jensen.dk/posts/2021-microk8s-with-traefik-and-metallb/
Upgrade values:
```bash
microk8s helm3 upgrade traefik traefik/traefik -n traefik --values traefik-values.yaml
```
### Setup Load Balancer
https://www.robert-jensen.dk/posts/2021-microk8s-with-traefik-and-metallb/
microk8s enable metallb:192.168.20.50-192.168.20.99

### Ingress
https://pacroy.medium.com/single-node-kubernetes-on-home-lab-using-microk8s-metallb-and-traefik-7bb1ea38fcc2

## Configure CI/CD access to cluster

### Setup GitLab cluster agent
Cluster agent can be used to access the cluster in GitLab CI/CD pipelines
```bash
# https://docs.gitlab.com/ee/user/clusters/agent/install/#installing-the-agent-for-kubernetes

# Follow instuctions after "Connect a cluster (agent)"
# prefix helm3 commands with microk8s e.g.
microk8s helm3 repo update

# WARNING! Add tailing backslash to wss://kas.gitlab.com/ at https://forum.gitlab.com/t/gitlab-com-premium-integrating-eks-with-gitlab-kubernetes-agent-agent-cant-reach-wss-address/59376 
```

### Update cluster agent
```bash
# Follow https://docs.gitlab.com/ee/user/clusters/agent/install/index.html#update-the-agent-version
```

### Install gitlab runner in cluster (untested)
```bash
# https://docs.gitlab.com/runner/install/operator.html#install-on-kubernetes
```

## Setup additional kubernetes services / tools

### Setup Prometheus
```bash
# Follow https://www.server-world.info/en/note?os=Ubuntu_20.04&p=microk8s&f=8
```

### Traefik Dashboard
https://faun.pub/securing-access-to-traefik-v2-dashboard-on-kubernetes-using-basic-authentication-b9535063f6e8


## Troubleshooting

### Error in name resolution
```bash
sudo systemctl restart systemd-resolved.service
```

